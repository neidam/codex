﻿using System.Collections;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine;


public class PostProcessManager : MonoBehaviour {

    public static PostProcessManager Instance;

    public float chromaticAberrationEndingSpeed;

    PostProcessVolume postProcessVolume;
    ChromaticAberration chromaticAberration;

    float progressChromaticAberration;

    public delegate void EndingEffect();
    public event EndingEffect OnEndingChromaticAberration;

    private void Awake() {
        if (!Instance) {
            Instance = this;
        }
    }

    void Start () {
        postProcessVolume = GetComponent<PostProcessVolume>();
        postProcessVolume.profile.TryGetSettings(out chromaticAberration);

        chromaticAberrationEndingSpeed = 1 / chromaticAberrationEndingSpeed;
	}

    public void StartEndingEffects() {
        StartCoroutine(EndingChromaticAberration());
    }

    public IEnumerator EndingChromaticAberration() {
        while (progressChromaticAberration < 1) {
            progressChromaticAberration += chromaticAberrationEndingSpeed * Time.deltaTime;
            chromaticAberration.intensity.value = progressChromaticAberration;
            yield return null;
        }
        OnEndingChromaticAberration();
    }

}
