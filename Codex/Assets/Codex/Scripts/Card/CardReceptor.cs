﻿using UnityEngine;
using UnityEngine.Events;

public class CardReceptor : MonoBehaviour {

    public LockableAugmented objectToUnlock;
    public UnityEvent onUnlock;

    public void Unlock() {
        onUnlock.Invoke();
    }
}
