﻿using UnityEngine;

/*
 * InputHandler
 * Base class that handle the inputs.
 */

public abstract class InputHandler : MonoBehaviour
{

    /*
     * Members
     */

    public static InputHandler Instance;

    Camera cam;                 // The camera used for the raycast. 
    Ray ray;                    // The raycast.
    RaycastHit hit;             // The hit point of the raycast.
    GameObject gObjectHit;      // The object hit by the raycast

    TrackableEventHandler trackableEventHandler;
    Tap tap;                    // Tap hit by the raycast.
    Ring ring;                  // Ring hit by the raycast.
    RotateObject rotateObject;  // RotateObject hit by the raycast.

    Card selectedCard;                  // The selected card.
    Ring selectedRing;                  // The selected ring to rotate.
    RotateObject selectedRotateObject;  // The selected RotateObject to interact with.


    Vector2 screenInputPosition;    // The screen position of the input.

    /*
     * Properties
     */

    public Camera Cam
    {
        get { return cam; }
        set { cam = value; }
    }

    public Card SelectedCard
    {
        get { return selectedCard; }
        set { selectedCard = value; }
    }

    /*
     * Methods
     */

    private void Start() {
        if (Instance == null) {
            Instance = this;
        }
        else {
            Destroy(this);
        }
    }

    // Search the object clicked or touched.
    protected void Raycast(Vector2 screenPosition) {
        screenInputPosition = screenPosition;
        ring = null;
        gObjectHit = null;
        ray = cam.ScreenPointToRay(screenPosition);
        hit = new RaycastHit();

        if (Physics.Raycast(ray, out hit)) {
            gObjectHit = hit.transform.gameObject;
            if (gObjectHit.layer == LayerMask.NameToLayer("Touchable")) {
                ring = gObjectHit.GetComponent<Ring>();
                rotateObject = gObjectHit.GetComponent<RotateObject>();
                tap = gObjectHit.GetComponent<Tap>();
            }
        }
    }

    // Actions to do when input is down.
    protected void HandleInputDown() {
        if (tap) {
            tap.BeginTap();
        }
        if (ring) {
            ring.BeginRotate(hit.point);
            selectedRing = ring;
        }
        if (rotateObject && !selectedCard) {
            rotateObject.BeginRotate(screenInputPosition);
            selectedRotateObject = rotateObject;
        }
    }

    // Actions to do when input drags.
    protected void HandleInputDrag() {
        if (selectedRing) {
            selectedRing.Rotate(hit.point);
        }
        if (selectedRotateObject) {
            selectedRotateObject.Rotate(screenInputPosition);
        }
    }

    // Action to do when input ends.
    protected void HandleInputEnd() {
        if (selectedRing) {
            selectedRing.EndRotate();
            selectedRing = null;
        }
        if (selectedRotateObject) {
            selectedRotateObject = null;
        }
        if (tap) {
            tap.EndTap();
        }
        if (selectedCard) {
            selectedCard.ReleaseCard(gObjectHit);
        }
    }
}
