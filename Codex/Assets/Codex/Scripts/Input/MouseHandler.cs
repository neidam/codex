﻿using UnityEngine;

/*
 * MouseHandler
 * Handle the inputs on editor mode.
 */

public class MouseHandler : InputHandler
{
    private void Update() {
        if (Input.GetMouseButtonDown(0)) {
            Raycast(Input.mousePosition);
            HandleInputDown();
        }
        else if (Input.GetMouseButton(0)) {
            Raycast(Input.mousePosition);
            HandleInputDrag();
        }
        else if (Input.GetMouseButtonUp(0)) {
            Raycast(Input.mousePosition);
            HandleInputEnd();
        }
    }
}
