﻿using UnityEngine;
using UnityEngine.Events;

public class Tap : MonoBehaviour {

    /*
     * Members
     */

    public bool selectOnce;
    public UnityEvent events;   // List on events to play when the object is selected
    public AK.Wwise.Event tap;

    bool selected;
    float threshold = 0.3f;
    float beginTap;

    /*
     * Methods
     */

    // Actions to play when selected
    public void LaunchEvents() {
        if (!selectOnce || (selectOnce && !selected)) {
            events.Invoke();
            selected = true;
        }
    }

    public void BeginTap() {
        beginTap = Time.time;
    }

    public void EndTap() {
        float tapDuration = Time.time - beginTap;
        if (tapDuration < threshold) {
            LaunchEvents();
            SoundManager.PlayEvent(tap);
        }
    }

}
