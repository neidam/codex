﻿using UnityEngine;

/*
 * TouchHandler
 * Handle the touch on mobile.
 */

public class TouchHandler : InputHandler
{
    void Update() {
        if (Input.touchCount > 0) {
            Touch touch = Input.GetTouch(0);
            Raycast(touch.position);
            if (touch.phase == TouchPhase.Began) {
                HandleInputDown();
            }
            else if (touch.phase == TouchPhase.Moved) {
                HandleInputDrag();
            }
            else if (touch.phase == TouchPhase.Ended) {
                HandleInputEnd();
            }
        }
    }
}
