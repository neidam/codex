﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager Instance;

    public Camera cam;

    public float minutesBeforeExtinction = 10;

    
    [Header("Menu")]
    public bool playMenu = false;
    public IntroPanel menuPanel;

    [Header("Introduction")]
    public bool playIntro = false;
    public IndicationTextUI introTextUI;
    public GameObject textIndicationToDelete;
    public TextAsset afterIntroText;


    [Header("Scenes to load additive")]
    public List<AugmentedObject> augmentedWithAdditiveScene;

    [Header("Wwise")]
    public AK.Wwise.Event eventMusic;
    public AK.Wwise.State beginMusic;

    int dataRetrieved = 0;

    public string sceneLoaded = "";

    public string SceneLoaded
    {
        get { return sceneLoaded; }
        set { sceneLoaded = value; }
    }

    public enum GameState
    {
        Menu,
        IntroText,
        Playing,
        Ending
    }

    GameState state = GameState.Menu;

    public GameState State
    {
        get { return state; }
        set { state = value; }
    }

    public int DataRetrieved
    {
        get { return dataRetrieved; }
    }

    private void Awake() {
        if (Instance == null) {
            Instance = this;
        }
        else {
            Destroy(gameObject);
        }
    }

    void Start() {
        InputHandler inputHandler;
        
        if (Application.isEditor) {
            inputHandler = gameObject.AddComponent<MouseHandler>();
        }
        else {
            inputHandler = gameObject.AddComponent<TouchHandler>();
        }
        inputHandler.Cam = cam;
        
        
        if (playMenu) {
            menuPanel.OnIntroPanelsOpened += DisplayAfterIntroText;
        }
        else if (playIntro){
            state = GameState.IntroText;
            menuPanel.gameObject.SetActive(false);
            DisplayAfterIntroText();
            SoundManager.PlayEvent(eventMusic);
            SoundManager.SetState(beginMusic);
        }
        else {
            Destroy(menuPanel.gameObject);
            Destroy(textIndicationToDelete);
            state = GameState.Playing;
            SoundManager.PlayEvent(eventMusic);
            SoundManager.SetState(beginMusic);
        }

        StartCoroutine(LoadAdditiveScenes());   

    }


    private void DisplayAfterIntroText() {
        Destroy(menuPanel.gameObject);
        state = GameState.IntroText;

        introTextUI.DisplayNewText(afterIntroText);
        introTextUI.OnPanelRemoved += StartExtinctionTimer;

        SoundManager.PlayEvent(eventMusic);
        SoundManager.SetState(beginMusic);
    }

    public IEnumerator LoadAdditiveScenes() {
        for (int i = 0; i < augmentedWithAdditiveScene.Count; i++) {
            yield return StartCoroutine(augmentedWithAdditiveScene[i].LoadSceneCoroutine());
        }
    }

    public void NewDataRetrieved(float delay = 0) {
        dataRetrieved++;
        UIManager.Instance.DataRetrieved(delay);
    }

    public void StartExtinctionTimer() {
        StartCoroutine(WaitBeforeExtinction());
        Destroy(textIndicationToDelete);
    }

    public IEnumerator WaitBeforeExtinction() {
        yield return new WaitForSeconds(minutesBeforeExtinction * 60);
        UIManager.Instance.StartEnding();
        state = GameState.Ending;
    }

    public IEnumerator WaitBeforePreventExtinction() {
        yield return new WaitForSeconds(minutesBeforeExtinction - 2 * 60);
        UIManager.Instance.StartEnding();
        state = GameState.Ending;
    }




}
