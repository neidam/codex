﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Vuforia;

/*
 * TrackableEventHandler
 * Custom implementation of ITrackableEventHandler
 */

public class TrackableEventHandler : MonoBehaviour, ITrackableEventHandler
{
    public bool needTouchToActivate = false;
    public bool needToBeInCenter;

    protected TrackableBehaviour mTrackableBehaviour;

    AugmentedObject[] augmentedObjects;

    Collider colliderTarget;

    bool touched;


    private Hashtable trackables = new Hashtable();

    private static string currentTrackableName = "";
    private static bool loaded = false;
    private static bool working = false;
    public static bool lostQueued = false;

    private const int SCENE_NAME = 0;
    private const int MODEL_NAME = 1;
    private const int IMAGE_TARGET_NAME = 2;


    private void Awake() {
        if (needTouchToActivate) {
            colliderTarget = GetComponent<Collider>();
        }
        augmentedObjects = GetComponentsInChildren<AugmentedObject>();
        
    }

    

    protected virtual void Start() {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
            mTrackableBehaviour.RegisterTrackableEventHandler(this);

        trackables.Add("", new string[] { "", "ToLoad", "" });

        for (int i = 0; i < augmentedObjects.Length; i++) {
            augmentedObjects[i].ActivateAugmented(false);
        }
    }

    protected virtual void OnDestroy()
    {
        if (mTrackableBehaviour)
            mTrackableBehaviour.UnregisterTrackableEventHandler(this);
    }

    
    public void OnTrackableStateChanged(
        TrackableBehaviour.Status previousStatus,
        TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                 newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            OnTrackingLost();
        }
        else
        {
            OnTrackingLost();
        }
    }

    protected virtual void OnTrackingFound()
    {
        if (GameManager.Instance.State != GameManager.GameState.Playing) {
            return;
        }

        if (needTouchToActivate && !touched) {
            return;
        }

        for (int i = 0; i < augmentedObjects.Length; i++) {
            if (augmentedObjects[i] == null) {
                continue;
            }
            augmentedObjects[i].TargetIsTracked = true;
            LockableAugmented lockable = augmentedObjects[i].GetComponent<LockableAugmented>();
            if ((lockable && !lockable.Locked) || !lockable) {
                if (needToBeInCenter) {
                    augmentedObjects[i].C = StartCoroutine(augmentedObjects[i].CheckIfInCenter());
                }
                else {
                    augmentedObjects[i].ActivateAugmented(true);
                }
            }
        }

        if (working || loaded || lostQueued)
            return;

        working = true;
        loaded = true;

       

        foreach (DictionaryEntry t in trackables) {
            if (mTrackableBehaviour.TrackableName == t.Key.ToString()) {
                string[] sa = (string[])t.Value;
                currentTrackableName = t.Key.ToString();
                StartCoroutine(LoadLevel(sa[SCENE_NAME], sa));
            }
        }
    }

    IEnumerator LoadLevel(string levelName, string[] levelData) {
        AsyncOperation async = SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Additive);
        yield return async;
        LoadLevelData(levelData[MODEL_NAME], levelData[IMAGE_TARGET_NAME]);
        working = false;
    }


    public void TargetTouched() {
        touched = true;
        colliderTarget.enabled = false;
        GetComponent<ParticleSystem>().Stop();
        OnTrackingFound();
    }


    protected virtual void OnTrackingLost()
    {
        for (int i = 0; i < augmentedObjects.Length; i++) {
            
            if (augmentedObjects[i] == null) {
                continue;
            }
            augmentedObjects[i].TargetIsTracked = false;
            if (augmentedObjects[i].IsActive) {
                augmentedObjects[i].LaunchEventsOnLostTrack();
            }
            augmentedObjects[i].ActivateAugmented(false);
            if (augmentedObjects[i].C != null) {
                StopCoroutine(augmentedObjects[i].C);
            }
        }
    }

    private void LoadLevelData(string objectName, string parentName) {
        GameObject go = GameObject.Find(objectName);

        if (go != null) {
            SetParent(go, parentName);
        }
    }

    private void SetParent(GameObject child, string parentName) {
        GameObject parent = GameObject.Find(parentName + " Plane");
        if (parent != null && child != null) {
            child.transform.parent = parent.transform;
            child.transform.position = parent.transform.position;
        }
    }

    private void DestroyLevelData(string objectName) {
        GameObject go = GameObject.Find(objectName);
        if (go != null) {
            Destroy(go);
        }
    }
}
