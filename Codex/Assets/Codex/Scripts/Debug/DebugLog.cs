﻿using UnityEngine;
using UnityEngine.UI;

/*
 * DebugLog
 * A class used of debug. Only displays debug logs on the screen.
 */

public class DebugLog : MonoBehaviour
{

    /*
     * Members
     */
    
    public static DebugLog Instance;    // Singleton.

    public GameObject debugPanel;       // The debug panel that appear when the toggled is on.
    public Transform debugTexts;        // GameObject that contains the logs.
    public GameObject originalText;     // Log for the original position. Destroy on start.
    public GameObject textPrefab;       // The text prefabs instanciated when a log is printed.
        
    bool visible = false;               // If the debug log is visible.
    Vector3 originalPosition;           // The original position of the first log.
    float textHeight;                   // The separation between two logs.

    int textCount = 0;                  // The number of logs.

    bool inEditor;                      // If the game is launched in editor.

    /*
     * Methods
     */

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            debugPanel.SetActive(visible);
            originalPosition = originalText.GetComponent<RectTransform>().position;
            textHeight = originalText.GetComponent<RectTransform>().rect.height + 10;
            originalText.SetActive(false);

            if (Application.isEditor)
                inEditor = true;
        }
        else
        {
            Destroy(this);
        }
    }

    // Change the active parameter of the debug panel.
    public void ChangeVisibility(bool change)
    {
        visible = change;
        debugPanel.SetActive(visible);
    }

    // Print a text in the debug log.
    public void Print(string text)
    {
        if (inEditor)
        {
            Debug.Log(text);
        }
        else
        {
            GameObject textObject = Instantiate(textPrefab);
            textObject.transform.SetParent(debugTexts);
            textObject.GetComponent<RectTransform>().position = new Vector3(originalPosition.x, originalPosition.y - textHeight * textCount, originalPosition.z);
            textObject.GetComponent<Text>().text = text;
            textObject.SetActive(true);
            textCount++;
        }
        
    }

    // Clear all logs.
    public void ClearLog()
    {
        foreach (Transform child in debugTexts)
        {
            Destroy(child.gameObject);
        }
        textCount = 0;
    }
}
