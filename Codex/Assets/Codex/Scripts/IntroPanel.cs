﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class IntroPanel : MonoBehaviour {

    public Image backgroundPanel;

    public GameObject menuPanel;

    public AK.Wwise.Event beginButtonPressed;

    [Header("Loading Text")]
    public TextMeshProUGUI loadingText;
    public TextAsset loadingTextAsset;


    public float fadingTextSpeed;
    public float panelOpenningSpeed;

    float progress;

    public delegate void EndingIntro();
    public event EndingIntro OnIntroPanelsOpened;

    public enum IntroState{
        menu,
        displayingIntro,
        fadingText,
        fadingPanel,
        inGame,
    }

    IntroState introState = IntroState.menu;

    private void Start() {
        fadingTextSpeed = 1f / fadingTextSpeed;
        panelOpenningSpeed = 1f / panelOpenningSpeed;
    }

    public void StartLoadingText() {
        menuPanel.SetActive(false);

        SoundManager.PlayEvent(beginButtonPressed);
        
        // Display loading text
        introState = IntroState.displayingIntro;
        loadingText.gameObject.SetActive(true);
        DisplayTextLetterByLetter displayTextLetterByLetter = loadingText.GetComponent<DisplayTextLetterByLetter>();
        displayTextLetterByLetter.StartDisplayText(System.Text.Encoding.Default.GetString(loadingTextAsset.bytes));
        displayTextLetterByLetter.OnEndingDisplay += BeginFadeText;
    }

    private void Update() {
        if (introState == IntroState.fadingText) {
            progress += fadingTextSpeed * Time.deltaTime;
            float newAlpha = Mathf.Lerp(1, 0, progress);
            loadingText.color = new Color(loadingText.color.r, loadingText.color.g, loadingText.color.b, newAlpha);

            if (progress >= 1) {
                progress = 0;
                introState = IntroState.fadingPanel;
            }
        }
        else if (introState == IntroState.fadingPanel) {
            progress += panelOpenningSpeed * Time.deltaTime;
            
            float newAlpha = Mathf.Lerp(1, 0, progress);
            backgroundPanel.color = new Color(backgroundPanel.color.r, backgroundPanel.color.g, backgroundPanel.color.b, newAlpha);

            if (progress >= 0.9) {
               introState = IntroState.inGame;
                OnIntroPanelsOpened();
                Destroy(gameObject);
            }
        }
    }

    public void BeginFadeText()
    {
        progress = 0;
        introState = IntroState.fadingText;
    }
    
}
