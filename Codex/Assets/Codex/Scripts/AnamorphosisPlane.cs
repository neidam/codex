﻿using UnityEngine;
using UnityEngine.Events;

public class AnamorphosisPlane : MonoBehaviour {


    bool validated;

    public bool Validated
    {
        get { return validated; }
    }

    public void Validate() {
        validated = true;
    }

}
