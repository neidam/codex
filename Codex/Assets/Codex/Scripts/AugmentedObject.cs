﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

/*
 * AugmentedObject
 * An augmented object is an object that appear when a target is detected by Vuforia.
 */

[System.Serializable]
public class AugmentedObject : MonoBehaviour {

    /*
     * Members
     */

    public UnityEvent OnTrack;

    public UnityEvent OnLostTrack;

    public UnityEvent OnFirstScan;

    public bool burn;
    public float burningDuration = 2f;

    public string sceneToLoad = "";

    [Header("Wwise")]
    public AK.Wwise.Event eventOnTrack;
    public AK.Wwise.Event eventOnTrack2;
    public AK.Wwise.State stateOnTrack;
    public AK.Wwise.Event eventOnLostTrack;
    public AK.Wwise.State stateOnLostTrack;

    bool targetIsTracked;   // If the target is detected
    bool isActive;          // If the object is activated

    List<Burn> objectsToBurn;
    float burningProgress = 1f;
    float burningTarget;

    [SerializeField]
    [HideInInspector]
    Chapter chapter;    // The chapter that the augmented object belongs to. 

    const string toBurnTag = "RendererToBurn";
    const string shaderProgressVariable = "Vector1_564E76A0";

    bool burningComplete;
    bool scannedOnce;

    bool loaded;

    Transform originalTransform;
    Transform targetOriginalTransform;



    Coroutine c;

    GameObject sceneLoaded;

    /*
     * Properties
     */

    public bool TargetIsTracked
    {
        get { return targetIsTracked; }
        set { targetIsTracked = value; }
    }

    public bool IsActive
    {
        get { return isActive; }
    }

    public Chapter Chapter
    {
        get { return chapter; }
        set { chapter = value; }
    }

    public bool BurningComplete
    {
        get { return burningComplete; }
        set { burningComplete = value; }
    }

    public Coroutine C
    {
        get { return c; }
        set { c = value; }
    }

    /*
     * Methods
     */

    public void Awake() {
        if (sceneToLoad != "") {
            originalTransform = transform;
        }
        objectsToBurn = new List<Burn>(GetComponentsInChildren<Burn>(true));
        for (int i = 0; i < objectsToBurn.Count; i++) {
            objectsToBurn[i].Augmented = this;
        }
        Burn burnSelf = GetComponent<Burn>();
        if (burnSelf) {
            objectsToBurn.Add(burnSelf);
        }
        burningDuration = 1f / burningDuration;
    }

    private void Update() {
        if (sceneToLoad != "") {
            transform.position = originalTransform.position;
            transform.rotation = originalTransform.rotation;
            transform.parent.transform.position = originalTransform.position;
            transform.parent.transform.rotation = originalTransform.rotation;
        }
    }

    public IEnumerator LoadSceneCoroutine() {
 
        AsyncOperation async = SceneManager.LoadSceneAsync(sceneToLoad, LoadSceneMode.Additive);
        yield return async;
        
        GameObject go = GameObject.Find("ToLoad");
        sceneLoaded = go;
        go.name = sceneToLoad;
        go.SetActive(false);
        loaded = true;
    }

    // Activate or deactivate the object.
    public void ActivateAugmented(bool activate) {
        isActive = activate;
        gameObject.SetActive(activate);
        if (activate) {
            SoundManager.PlayEvent(eventOnTrack);
            SoundManager.PlayEvent(eventOnTrack2);
            SoundManager.SetState(stateOnTrack);
            OnTrack.Invoke();
            if (!scannedOnce) {
                scannedOnce = true;
                OnFirstScan.Invoke();
            }
            if (burn) {
                BurnObject(true);
            }
            if (sceneToLoad != "" && sceneLoaded) {
                sceneLoaded.SetActive(true);
            }
        }
        else {
            StopCoroutine("Burn");
        }
    }

    public void LoadScene() {
        /*
        if (GameManager.Instance && GameManager.Instance.SceneLoaded != sceneToLoad && sceneToLoad != "") {
            if (GameManager.Instance.SceneLoaded != "") {
                SceneManager.UnloadSceneAsync(GameManager.Instance.SceneLoaded);
            }
            StartCoroutine(LoadSceneCoroutine());
            GameManager.Instance.SceneLoaded = sceneToLoad;
        }*/
    }

   
    public IEnumerator CheckIfInCenter() {
        bool canBeAugmented = false;
        while (!canBeAugmented) {
            Vector3 viewPos = GameManager.Instance.cam.WorldToViewportPoint(transform.position);
            if (viewPos.x < 0.2 || viewPos.x > 0.8 || viewPos.y < 0.2 || viewPos.y > 0.8) {
                canBeAugmented = false;
            }
            else {
                canBeAugmented = true;
            }
            yield return null;
        }
        ActivateAugmented(true);
        
    }

    


    public void LaunchEventsOnLostTrack() {
        SoundManager.PlayEvent(eventOnLostTrack);
        SoundManager.SetState(stateOnLostTrack);
        if (sceneToLoad != "" && sceneLoaded) {
            sceneLoaded.SetActive(false);
        }
        OnLostTrack.Invoke();
    }

    public void BurnObject(bool appear){
        for (int i = 0; i < objectsToBurn.Count; i++) {
            if (objectsToBurn[i].gameObject.activeInHierarchy) {
                objectsToBurn[i].StartBurn(appear);
            }
        }
    }

  

    

    public bool IsBurned() {
        return burningComplete;
    }

    public void SetChapterMusicState() {
        SoundManager.SetState(chapter.enigmaMusic);
    }

    public void RemoveStateOnTrack() {
        stateOnTrack = null;
    }
}
