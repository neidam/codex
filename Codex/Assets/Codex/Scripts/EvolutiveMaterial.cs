﻿using System.Collections.Generic;
using UnityEngine;

public class EvolutiveMaterial : MonoBehaviour {

    public List<Material> materials;

    [Header("Wwise")]
    public AK.Wwise.Event rightSymbolScanned;
    public AK.Wwise.Event wrongSymbolScanned;
    public AK.Wwise.Event allSymbolsScanned;
    public AK.Wwise.Event apparition = null;
    public float apparitionSoundDelay;

    MeshRenderer meshRenderer;
    int state = 0;

    public List<GameObject> wrongSymbols;


    public void Start() {
        
        if (!meshRenderer) {
            meshRenderer = GetComponent<MeshRenderer>();
        }
        if (state == -1) {
            meshRenderer.enabled = false;
            meshRenderer.material = null;
        }
    }

    public void OnTrack() {
        if (state >= 0) {
            SoundManager.PlayEvent(apparition);
        }
    }

    public void TryIncreaseState(InteractiveButton button) {
        if (button.symbolIsRight && !button.Validated) {
            
            if (state == materials.Count - 1) {
                return;
            }
            else if (!meshRenderer) {
                meshRenderer = GetComponent<MeshRenderer>();
            }
            state++;
            meshRenderer.enabled = true;
            meshRenderer.material = materials[state];
            button.Validated = true;

            if (state < materials.Count - 1) {
                SoundManager.PlayEvent(rightSymbolScanned);
            }
            else {
                for (int i = 0; i < wrongSymbols.Count; i++) {
                    Destroy(wrongSymbols[i]);
                }
                SoundManager.PlayEvent(allSymbolsScanned);
                LockableAugmented lockableAugmented = GetComponent<LockableAugmented>();
                if (lockableAugmented) {
                    lockableAugmented.Validate();
                }
            }
            if (apparition != null) {
                //SoundManager.PlayEventWithDelay(apparition, apparitionSoundDelay);
            }
        }
        else if (!button.symbolIsRight) {
            SoundManager.PlayEvent(wrongSymbolScanned);
        }

    }
}
