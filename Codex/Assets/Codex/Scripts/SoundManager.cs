﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public static SoundManager Instance;

    public const string musicGroup = "Interactive_Music";

    static AK.Wwise.State currentState = null;

    private void Awake() {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad(Instance);
          
        }
        else {
            Destroy(this);
        }
    }

    public void PlaySound(string soundName) {
        AkSoundEngine.PostEvent(soundName, gameObject);
    }

    public void SetMusicState(string state) {
        AkSoundEngine.SetState(musicGroup, state);
    }

    public static void PlayEvent(AK.Wwise.Event wwiseEvent) {
        if (wwiseEvent != null) {
            wwiseEvent.Post(Instance.gameObject);
        }
    }

    public static void PlayEventWithDelay(AK.Wwise.Event wwiseEvent, float delay) {
        Instance.StartCoroutine(PlayEventCoroutine(wwiseEvent, delay));
    }

    public static IEnumerator PlayEventCoroutine(AK.Wwise.Event wwiseEvent, float delay) {
        yield return new WaitForSeconds(delay);
        PlayEvent(wwiseEvent);
    }

    public static void SetState(AK.Wwise.State wwiseState) {
        if (wwiseState.ID != 0 && wwiseState != currentState) {
            wwiseState.SetValue();
            currentState = wwiseState;
        }
    }
}
