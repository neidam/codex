﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
/*
public class Anamorphosis : MonoBehaviour {

    public GameObject BrokenRune;
    public GameObject RuneToActivate;
    
    public GameObject plane;

    
    public UnityEvent onValidate;
    
    Camera cam;

    Ray ray;
    RaycastHit hit;
    AnamorphosisPlane planeHit;

    int progressIndex = 0;
    
	// Use this for initialization
	void Start () {
        cam = Camera.main;
        planes = GetComponentsInChildren<AnamorphosisPlane>().ToList();
        for (int i = 0; i < planes.Count; i++) {
        }
	}

    private void Update() {
 
        ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        if (Physics.Raycast(ray, out hit, LayerMask.GetMask("AnamorphosisPlane"))) {
            planeHit = hit.transform.gameObject.GetComponent<AnamorphosisPlane>();

            if ((ValidateInOrder && planes[progressIndex] != planeHit) || planeHit == null) {
                return;
            }
            else if (ValidateInOrder) {
                progressIndex++;
            }

            if (!planeHit.Validated) {
                Vector3 incomingVec = hit.point - cam.transform.position;
                float angle = Vector3.Angle(incomingVec, -planes[0].transform.up);
                Debug.Log(angle);
                if (angle < planeHit.minValidationAngle) {
                    planeHit.Validate();
                    CheckIfValidated();
                }
            }
        }
       
        
        Vector3 pointOnPlane = Vector3.ProjectOnPlane(cam.transform.position, planes[0].GetComponent<Plane>().normal);
        Debug.Log(pointOnPlane);
        // Vector way
        if (!ValidateInOrder) {
            for (int i = 0; i < planes.Count; i++) {
                TryValidatePlane(i);
            }
        }
        else if (TryValidatePlane(progressIndex)) {
                progressIndex++;
            }
         
        }

    private bool TryValidatePlane(int index) {



        float angle = Vector3.Angle(cam.transform.forward, -planes[index].transform.up);
        if (angle < planes[index].minValidationAngle) {
            planes[index].Validate();
            CheckIfValidated();
            return true;
        }
        return false;
    }

    private bool CheckIfValidated() {
        for (int i = 0; i < planes.Count; i++) {
            if (!planes[i].Validated) {
                return false;
            }
        }
        onValidate.Invoke();
        Debug.Log("AH");
        return true;
    }


}*/
