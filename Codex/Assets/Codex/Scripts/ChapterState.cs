﻿/*
 * ChapterState
 * The states of a chapter
 */

public enum ChapterState
{
    CannotBeOpened,
    CanBeOpened,
    Opened,
    Finished
}