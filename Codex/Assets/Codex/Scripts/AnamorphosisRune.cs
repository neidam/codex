﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class AnamorphosisRune : MonoBehaviour {

    public GameObject BrokenRune;
    public GameObject RuneToActivate;
    
    public GameObject plane;

    public float minValidationAngle = 5;

    public AK.Wwise.Event anamorphosisResolved;
    public AK.Wwise.Event anamorphosisAppear;

    bool validated = false;

    public GameObject revealedText;

    public UnityEvent onValidate;

    public bool dissolveCompleteRune = true;
    public bool removeText = true;
    
    Camera cam;

    Ray ray;
    RaycastHit hit;
    GameObject planeHit;

    List<Burn> brokenBurn;
    
    
	void Start () {
        cam = Camera.main;
        brokenBurn = new List<Burn>();
        for (int i = 0; i < BrokenRune.transform.childCount; i++) {
            Burn burn = BrokenRune.transform.GetChild(i).GetComponent<Burn>();
            if (burn) {
                brokenBurn.Add(burn);
            }
        }
        RuneToActivate.SetActive(false);
    }

    private void Update() {
        if (validated) {
            return;
        }

        ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        if (Physics.Raycast(ray, out hit, LayerMask.GetMask("AnamorphosisPlane"))) {
            planeHit = hit.transform.gameObject;
            Vector3 incomingVec = hit.point - cam.transform.position;

            float angle = Vector3.Angle(-incomingVec, plane.transform.up);
            if (angle < minValidationAngle) {
                Validate();
            }
        }
    }

    public void OnTrack() {
        if (!validated) {
            SoundManager.PlayEvent(anamorphosisAppear);
        }
    }

    private bool Validate() {
        validated = true;
        BurnReverseBroken();
        SoundManager.PlayEvent(anamorphosisResolved);
        RuneToActivate.SetActive(true);
        onValidate.Invoke();

        return true;
    }

    public void BurnReverseBroken() {
        for (int i = 0; i < BrokenRune.transform.childCount; i++) {
            if (i < brokenBurn.Count && brokenBurn[i]) {
                brokenBurn[i].StartBurn(false);
            }
            else {
                BrokenRune.transform.GetChild(i).gameObject.SetActive(false);
            }
        }
        StartCoroutine(WaitBeforeDisplayText());
    }

    public IEnumerator WaitBeforeDisplayText() {
        yield return new WaitForSeconds(3f);
        DisplayText();
    }

    public void DisplayText() {
        if (dissolveCompleteRune) {
            Burn completeBurn = RuneToActivate.GetComponent<Burn>();
            if (completeBurn) {
                completeBurn.StartBurn(false);
            }
            else {
                RuneToActivate.SetActive(false);
            }
        }
        if (revealedText) {
            revealedText.gameObject.SetActive(true);
            Burn textBurn = revealedText.GetComponent<Burn>();
            if (textBurn) {
                textBurn.StartBurn(true);
            }
            StartCoroutine(WaitBeforeDisplayNewAnamorphosis());
        }
        else {
            GetComponent<LockableAugmented>().ValidateWithDelay(0f);
        }
    }

    public IEnumerator WaitBeforeDisplayNewAnamorphosis() {
        yield return new WaitForSeconds(3f);
        EraseText();
        
    }

    public void EraseText() {
        if (removeText) {
            Burn textBurn = revealedText.GetComponent<Burn>();
            if (textBurn) {
                textBurn.StartBurn(false);
            }
            else {
                revealedText.SetActive(false);
            }
        }
        
        GetComponent<LockableAugmented>().ValidateWithDelay(0f);

    }

}
