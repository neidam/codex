﻿public enum WheelPuzzleState
{
    Locked,
    Unlocked,
    Solved
}
