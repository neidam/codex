﻿using UnityEngine;
using System.Collections;
using Vuforia;
using UnityEngine.SceneManagement;

public class LoadLevelTrackableHandler : MonoBehaviour,
                                            ITrackableEventHandler
{

    private TrackableBehaviour mTrackableBehaviour;

    private Hashtable trackables = new Hashtable();

    private static string currentTrackableName = "";
    private static bool loaded = false;
    private static bool working = false;
    public static bool lostQueued = false;

    private const int SCENE_NAME = 0;
    private const int MODEL_NAME = 1;
    private const int IMAGE_TARGET_NAME = 2;

    void Start() {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();

        if (mTrackableBehaviour) {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }

        trackables.Add("", new string[] { "", "", "" }); //SCENE_NAME, MODEL_NAME (ROOT GAMEOBJECT), IMAGE_TARGET_NAME
        trackables.Add("", new string[] { "", "", "" });


        OnTrackingLost();
    }

    void Update() {
        if (lostQueued) {
            OnTrackingLost();
        }
    }

    public void OnTrackableStateChanged(
                                    TrackableBehaviour.Status previousStatus,
                                    TrackableBehaviour.Status newStatus) {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED) {
            OnTrackingFound();
        }
        else {
            OnTrackingLost();
        }
    }


    private void OnTrackingFound() {
        if (working || loaded || lostQueued)
            return;

        working = true;
        loaded = true;

        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>();
        foreach (Renderer component in rendererComponents) {
            component.enabled = true;
        }

        foreach (DictionaryEntry t in trackables) {
            if (mTrackableBehaviour.TrackableName == t.Key.ToString()) {
                string[] sa = (string[])t.Value;
                currentTrackableName = t.Key.ToString();
                StartCoroutine(LoadLevel(sa[SCENE_NAME], sa));
            }
        }
    }


    IEnumerator LoadLevel(string levelName, string[] levelData) {
        AsyncOperation async = SceneManager.LoadSceneAsync(levelName, LoadSceneMode.Additive);
        yield return async;
        LoadLevelData(levelData[MODEL_NAME], levelData[IMAGE_TARGET_NAME]);
        working = false;
    }

    private void OnTrackingLost() {
        if (working) {
            lostQueued = true;
            return;
        }

        lostQueued = false;
        working = true;

        if (!gameObject.activeSelf)
            return;

        foreach (DictionaryEntry t in trackables) {
            if (currentTrackableName == t.Key.ToString()) {
                string[] sa = (string[])t.Value;
                DestroyLevelData(sa[MODEL_NAME]);
            }
        }

        Renderer[] rendererComponents = GetComponentsInChildren<Renderer>();

        foreach (Renderer component in rendererComponents) {
            component.enabled = false;
        }

        System.GC.Collect();

        working = false;
        loaded = false;
    }

    private void LoadLevelData(string objectName, string parentName) {
        GameObject go = GameObject.Find(objectName);

        if (go != null) {
            SetParent(go, parentName);
        }
    }

    private void SetParent(GameObject child, string parentName) {
        GameObject parent = GameObject.Find(parentName + " Plane");
        if (parent != null && child != null) {
            child.transform.parent = parent.transform;
            child.transform.position = parent.transform.position;
        }
    }

    private void DestroyLevelData(string objectName) {
        GameObject go = GameObject.Find(objectName);
        if (go != null) {
            Destroy(go);
        }
    }
}