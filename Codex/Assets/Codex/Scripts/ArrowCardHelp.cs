﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowCardHelp : MonoBehaviour {

    public Transform cardBeginning;
    public Transform target;

    SpriteRenderer spriteRenderer;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 dir = cardBeginning.position - target.position;
        float distance = Vector3.Distance(target.position, target.position);

        Vector3 center = Vector3.Lerp(cardBeginning.position, target.position, 0.5f);
        transform.position = center;
        Vector2 newSize = new Vector2(distance, spriteRenderer.size.y);
        Vector2 newSizeCollider = new Vector2(distance - 0.8f, spriteRenderer.size.y - 0.5f);
        spriteRenderer.size = newSize;
        transform.rotation = Quaternion.FromToRotation(Vector3.right, dir);
    }
}
