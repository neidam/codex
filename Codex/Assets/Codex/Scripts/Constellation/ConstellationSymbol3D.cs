﻿using UnityEngine;

public class ConstellationSymbol3D : MonoBehaviour
{
    bool validated = false;

    public Constellation3D constellation;

    public MeshRenderer linkToActivate;

    MeshRenderer meshRenderer;

    public void Start() {
        meshRenderer = GetComponent<MeshRenderer>();
    }

    public void TryUnlock() {
        if (!constellation.Validated && constellation.symbolsToScan[constellation.IndexToUnlock] == this) {
            int index = constellation.SymbolIndex(this);
            if (index < constellation.symbolsToScan.Count - 1) {
                SoundManager.PlayEvent(constellation.rightSymbol);
            }
            else {
                SoundManager.PlayEvent(constellation.allSymbolScanned);
            }
            meshRenderer.material = constellation.symbolActivated;
            if (linkToActivate) {
                linkToActivate.material = constellation.symbolActivated;
            }
            validated = true;
            constellation.IndexToUnlock++;
            constellation.CheckIfValidated();
        }
        else if (!constellation.Validated && validated == false) {
            constellation.ResetSymbols();
        }
    }

    public void Unvalidate() {
        if (validated) {
            validated = false;
            meshRenderer.material = constellation.symbolUnactivated;
            if (linkToActivate) {
                linkToActivate.material = constellation.symbolUnactivated;
            }
        }
    }
}
