﻿Shader "Codex/NewSurfaceShader" {
	Properties {
		[Enum(Off,0,Front,1,Back,2)] _CullMode ("Culling Mode", int) = 0
		[Enum(Off,0,On,1)] _ZWrite("ZWrite", int) = 0
		_Progress("Progress",Range(0,1)) = 0		
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_DissolveTex("Dissolve Texture", 2D) = "white" {}
		_Edge("Edge",Range(0.01,0.5)) = 0.01

		[Header(Edge Color)]
		[Toggle(EDGE_COLOR)] _UseEdgeColor("Edge Color?", Float) = 1
		[HideIfDisabled(EDGE_COLOR)][NoScaleOffset] _EdgeAroundRamp("Edge Ramp", 2D) = "white" {}
		[HideIfDisabled(EDGE_COLOR)]_EdgeAround("Edge Color Range",Range(0,0.5)) = 0
		[HideIfDisabled(EDGE_COLOR)]_EdgeAroundPower("Edge Color Power",Range(1,5)) = 1
		[HideIfDisabled(EDGE_COLOR)]_EdgeAroundHDR("Edge Color HDR",Range(1,3)) = 1
		[HideIfDisabled(EDGE_COLOR)]_EdgeDistortion("Edge Distortion",Range(0,1)) = 0
	}
	SubShader {
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _MetallicTex;
		sampler2D _NormalTex;
		sampler2D _EmissiveTex;
		sampler2D _DissolveTex;

		
		float4 _DissolveTex_ST;

		struct Input {
			float2 uv_MainTex;
			float2 uv_NormalTex;
			float2 uv_EmissiveTex;
			float2 uv_DissolveTex;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		fixed _Edge;
		fixed _Progress;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			// Metallic and smoothness come from slider variables
			o.Albedo = c.rgb;
			fixed4 metal = tex2D(_MetallicTex, IN.uv_MainTex);
			o.Metallic = metal.r;
			o.Smoothness = metal.a * _Glossiness;
			o.Alpha = c.a;
			o.Normal = UnpackNormal (tex2D (_NormalTex, IN.uv_NormalTex));




			//float2 uv3 = IN.uv_DissolveTex.xy * _DissolveTex_ST.xy + _DissolveTex_ST.zw;
			//fixed4 col = tex2D(_DissolveTex, uv3);
			//fixed x = col.r;

			//Edge
			//fixed edge = lerp( x + _Edge, x - _Edge, _Progress);
			//fixed alpha = smoothstep(  _Progress + _Edge, _Progress - _Edge, edge);
		
			
			
		}
		ENDCG
	}
	FallBack "Diffuse"
}
