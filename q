[33mcommit c93d126685f91cc30771b9751ae9ffc5a84098db[m[33m ([m[1;36mHEAD -> [m[1;32mdevelopment[m[33m, [m[1;31morigin/development[m[33m, [m[1;31morigin/damien[m[33m, [m[1;32mdamien[m[33m)[m
Author: CRECHET Damien <Damien.crechet@formation.enjmin.fr>
Date:   Tue May 22 18:51:23 2018 +0200

    Integrated city scenes

[33mcommit 1e92462c761a58f17dd8b419c11b704bcfdbd71a[m[33m ([m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m, [m[1;32mmaster[m[33m)[m
Author: CRECHET Damien <Damien.crechet@formation.enjmin.fr>
Date:   Tue May 22 15:26:18 2018 +0200

    added feedbacks for enigma 1

[33mcommit 63ecb5ccf695fb1a322924cf08684375085621ae[m[33m ([m[1;31morigin/Thomas[m[33m, [m[1;32mThomas[m[33m)[m
Merge: a81da2f afd08ee
Author: CRECHET Damien <Damien.crechet@formation.enjmin.fr>
Date:   Tue May 22 13:25:32 2018 +0200

    merge

[33mcommit a81da2f87b31a3549a43224c4975da0a9438c41e[m
Author: CRECHET Damien <Damien.crechet@formation.enjmin.fr>
Date:   Tue May 22 12:15:41 2018 +0200

    WIP

[33mcommit afd08ee17dd9ad9abf0137940e9190f5fee3d288[m
Author: Saaamoth <thomas.luke.garcia@gmail.com>
Date:   Tue May 22 10:08:11 2018 +0200

    scenes

[33mcommit a26d19e8a3c43cac50261a663833e99f672149ff[m
Author: Althemar <damien.crechet@gmail.com>
Date:   Tue May 22 09:15:47 2018 +0200

    Added enigma music

[33mcommit e208389a082e3d8f2579e58500389f99518a832a[m
Author: Althemar <damien.crechet@gmail.com>
Date:   Tue May 22 02:43:00 2018 +0200

    Added additive 3D scene and ui text

[33mcommit 77d3c0a70c829f015cf376c3786845fce7eb037c[m
Author: Althemar <damien.crechet@gmail.com>
Date:   Mon May 21 01:45:08 2018 +0200

    More things

[33mcommit 4803adae8a5858a3a85676da161921e27439a2fc[m
Author: Althemar <damien.crechet@gmail.com>
Date:   Sun May 20 16:55:01 2018 +0200

    Added enigma 1 sounds

[33mcommit c449d07a0af0f4a1357bd5b51e580b131d86c76b[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Sat May 19 16:47:54 2018 +0200

    Integrated feedback sounds

[33mcommit fe4efd92e31a6b6d02e32348ee1d0236589f830d[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Fri May 18 18:01:06 2018 +0200

    Enigma 3 : Improve symbol detection

[33mcommit 71c841b079d94b61cc75c05d801875b1a8526218[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Thu May 17 16:01:27 2018 +0200

    wip

[33mcommit a13a5c99108df9b294f65d47b083c2ef9c40938a[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Tue May 15 16:34:02 2018 +0200

    Upgrade to 2018.1

[33mcommit 2245b9fc52f05b289b9586277327dfb1629ce719[m
Merge: aeb6cff 74943a6
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Mon May 14 14:00:37 2018 +0200

    merge

[33mcommit aeb6cff83493b1ae6605be095c13aac4bb062fe3[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Mon May 14 12:36:51 2018 +0200

    .gitignore modified

[33mcommit e7d85fd1097e7bbed64b3334518040e354b5eba4[m
Author: Althemar <damien.crechet@gmail.com>
Date:   Mon May 14 12:20:48 2018 +0200

    Added scene damien2, with enigma1, 2, and 3

[33mcommit 74943a6a5d9a0e12acb07449d8f2be7cd5eac8e0[m
Author: Saaamoth <thomas.luke.garcia@gmail.com>
Date:   Sat May 12 19:10:33 2018 +0200

    props

[33mcommit fdb4505426d6d512c13422e9fc09e12b9df2a4c0[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Thu May 3 18:46:04 2018 +0200

    push vacances

[33mcommit 9538f6f904113cd110b2bebfa89ec9b23fa1ef3d[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Thu May 3 12:53:14 2018 +0200

    Replace boat model

[33mcommit ffaf0939d51d1ab4a96503100b72ca8e7c264e21[m
Merge: 5bd9c28 4200d82
Author: Althemar <damien.crechet@gmail.com>
Date:   Thu May 3 11:34:08 2018 +0200

    Merge branch 'Thomas' into development

[33mcommit 5bd9c2890ebcc340dc1e9967693bc5c78751cf19[m
Author: Althemar <damien.crechet@gmail.com>
Date:   Thu May 3 11:31:45 2018 +0200

    Added hints

[33mcommit 40cbce381e6afafcb8a82c3aa056f653373f4e58[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Wed May 2 18:09:57 2018 +0200

    Added FPS counter, HandHelper for card, and began hints manager

[33mcommit 4200d82e81a27ff3be818a1100509cd24a6d5b84[m
Author: Saaamoth <thomas.luke.garcia@gmail.com>
Date:   Wed May 2 16:53:13 2018 +0200

    Runic pattern

[33mcommit 09d57022d69f0053d0b9bfc9b30803edb535111e[m
Author: Saaamoth <thomas.luke.garcia@gmail.com>
Date:   Wed May 2 15:03:54 2018 +0200

    fixed water + boat

[33mcommit b26edd10503700233bdd7f7f1dda4b2089143fb5[m
Author: Althemar <damien.crechet@gmail.com>
Date:   Tue May 1 17:53:05 2018 +0200

    wip

[33mcommit 58e5edddfed16932735230dbdd1c0362b6dd8e31[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Fri Apr 27 19:56:55 2018 +0200

    Added rotation with plane, many things

[33mcommit 5ed90bc63dbe9467c7b2d68834151d73b2a07a62[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Fri Apr 27 13:27:27 2018 +0200

    So many things

[33mcommit 5dd1cb246dd7ed8e7753b0cea9a2ee00c430b7b2[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Thu Apr 26 18:14:39 2018 +0200

    WIP

[33mcommit 1610440d957244bac4202ec2f8bb48a15c3b9461[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Thu Apr 26 17:41:52 2018 +0200

    test

[33mcommit b89a8966c18a62a7773b964af662e49366245e70[m
Merge: 7e0e846 342af8a
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Thu Apr 26 16:05:13 2018 +0200

    merge

[33mcommit 7e0e8465ec323f6730fbd6d6fd6c5f1a07c1b724[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Thu Apr 26 15:46:42 2018 +0200

    bug push ?

[33mcommit e3015221757f2deb58de53b26a51a5b8cbfdcee3[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Thu Apr 26 15:43:07 2018 +0200

    Sound added

[33mcommit 342af8a2facf860374b086854f5a80575663c35b[m
Author: Saaamoth <thomas.luke.garcia@gmail.com>
Date:   Thu Apr 26 15:26:57 2018 +0200

    FX + chara + boats

[33mcommit a51cf3cb6468608815cf9fd511140f8246cb5075[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Thu Apr 26 13:57:31 2018 +0200

    Sounds integrated

[33mcommit 2df6de47ce190a9ecdd5c6ea6d7f0666c0944d4b[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Wed Apr 25 20:15:10 2018 +0200

    Remove some useless files

[33mcommit 057a578f75e6ce5af8ec8dd0b5e80d9b3abbd966[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Wed Apr 25 19:44:40 2018 +0200

    Added gods and chapter 1 target. Set chapter 1 progression

[33mcommit 81a4750ba0e2a5a0b3a5d56408afc81f13c50c29[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Wed Apr 25 17:06:42 2018 +0200

    Added burn shader and other and WIP

[33mcommit fea5fc793a1d0fb446b09c7eed17f755cc8d8712[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Tue Apr 24 18:26:33 2018 +0200

    Cliff integrated in window

[33mcommit bd30e553ba0d3f3553e7c85b87ad1559b4fd34fb[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Tue Apr 24 16:25:18 2018 +0200

    Cliff integrated. Gitignore modified

[33mcommit 04756423e757753fc9dbd680f6034e1f97d92ff6[m
Merge: d1556dd d2d35a3
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Tue Apr 24 15:59:47 2018 +0200

    merge

[33mcommit d1556dda32564991c438ab24c5570ec113ce8610[m
Merge: 7f7de05 a051466
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Tue Apr 24 15:58:26 2018 +0200

    Merge branch 'damien' into development

[33mcommit a05146638e2ac13d3bf42aa3a4c858ac97a27537[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Tue Apr 24 15:54:45 2018 +0200

    Improve chapter progression with chapter beginning and chapter ending

[33mcommit d2d35a38b54ade829024039b3c1ec8a82e451a69[m
Author: Saaamoth <thomas.luke.garcia@gmail.com>
Date:   Tue Apr 24 14:48:19 2018 +0200

    entrance grid + hanging houses

[33mcommit d3206f7565bb05e6cb4d7b72b848579b9e11c486[m
Author: Althemar <damien.crechet@gmail.com>
Date:   Tue Apr 24 14:17:45 2018 +0200

    WIP

[33mcommit 1d847788fd2dad3c55409b51977cfea2b6552d95[m
Author: Althemar <damien.crechet@gmail.com>
Date:   Tue Apr 24 10:23:17 2018 +0200

    Modified ChapterEditor to use Property. Level design tool is almost working. Need more testing

[33mcommit 4dfad3706be22f953d297ea84b4db0fe27087cb7[m
Author: Saaamoth <thomas.luke.garcia@gmail.com>
Date:   Mon Apr 23 17:10:59 2018 +0200

    city + inner cavity added

[33mcommit 7f7de05be61bb8adf81723bf7ea3bbddbb681eae[m
Author: Althemar <damien.crechet@gmail.com>
Date:   Mon Apr 23 11:45:31 2018 +0200

    gitignore modified

[33mcommit 35bcac606598b5adc9d46cc987aa70ac8917867a[m
Merge: 1fc9305 2178f72
Author: Althemar <damien.crechet@gmail.com>
Date:   Mon Apr 23 11:42:24 2018 +0200

    Merge branch 'damien' into development

[33mcommit 1fc93055d88e9bb8e180191cc0659ff415c9c30d[m
Merge: 835532d 240ee96
Author: Althemar <damien.crechet@gmail.com>
Date:   Mon Apr 23 11:34:06 2018 +0200

    merge

[33mcommit 240ee96f5410321eced1c6f0bb58fc7b4cedd5ba[m
Author: Saaamoth <thomas.luke.garcia@gmail.com>
Date:   Sat Apr 21 12:13:52 2018 +0200

    fixed lights on test scene

[33mcommit 2178f72c0bfc984b71874af3ae0e4c3faf90fb8b[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Fri Apr 20 17:05:51 2018 +0200

    WIP

[33mcommit e84c4e461bea971f611f65c62ba28a26a793e774[m
Author: Saaamoth <thomas.luke.garcia@gmail.com>
Date:   Fri Apr 20 16:44:45 2018 +0200

    new city gate + tests with progressive light baker

[33mcommit 835532ddc4538f642d87e9544e2fd2993cdd0773[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Fri Apr 20 15:11:25 2018 +0200

    Added card spawn. Coroutines deleted to use state machine

[33mcommit ac6fb9fd946654f2bd0ea7f48b320bb7e8c07be1[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Thu Apr 19 15:53:39 2018 +0200

    Added tool for editing level design

[33mcommit 7dd2a9bc00919b77d30813e068e5abed94034090[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Wed Apr 18 18:51:53 2018 +0200

    Card fade

[33mcommit 88ffdbec12ea1530396db42c4e00424eaec5e6e6[m
Merge: 4591fdd ef6538d
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Wed Apr 18 16:20:08 2018 +0200

    Merge branch 'damien' into development

[33mcommit ef6538dc35cbebf3da884998516b119cd8a9ce48[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Wed Apr 18 15:36:18 2018 +0200

    Card validation

[33mcommit 4591fdd5be32cb0032ef385befaccd654255d797[m
Author: Saaamoth <thomas.luke.garcia@gmail.com>
Date:   Wed Apr 18 15:28:41 2018 +0200

    tests assets + test scene

[33mcommit fcef899172a8d8df103f3db58fd31c9765c6cfb1[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Wed Apr 18 14:58:17 2018 +0200

    Added cart scale modifier and position reset

[33mcommit 976136e9f3af03020ba254326b48e265999eed90[m
Author: Saaamoth <thomas.luke.garcia@gmail.com>
Date:   Tue Apr 17 18:31:01 2018 +0200

    first commit

[33mcommit fff694a0642f78a3b5016605591dbf796591943b[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Tue Apr 17 14:52:55 2018 +0200

    Card added

[33mcommit d29ede831c59d93c6b8ae013b8e094c1b810eb3a[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Fri Apr 13 18:20:25 2018 +0200

    Added anamorphosis

[33mcommit 1cd7c00808fddae2d5516b196200f1839b2f11f5[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Fri Apr 13 13:22:54 2018 +0200

    .gitignore modified: AkWwiseProjectData.asset is now ignored. Tap class added

[33mcommit cf56c240edcb206dd9c3aa54e793cd179ee8d3d2[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Thu Apr 12 18:29:21 2018 +0200

    Added mask rotation

[33mcommit f08e825a9801edc8b14bacee2b43249be983ea54[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Thu Apr 12 16:50:49 2018 +0200

    build again

[33mcommit dc8f713d4e7659a99c2f96dfd66128d6e4f0c127[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Thu Apr 12 16:21:41 2018 +0200

    build ubi encore

[33mcommit 6f9815a17a9153ba800e1fc42e9b3fadf282f6e0[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Thu Apr 12 14:25:15 2018 +0200

    Build ready for ubi

[33mcommit 119a27aeb765e0a66084f67a3baaef7150cf49d5[m
Author: Althemar <damien.crechet@gmail.com>
Date:   Thu Apr 12 12:17:15 2018 +0200

    JOHN ROMERO JUST SAW OUR GAME !

[33mcommit a6fd0bb63f45531f732b102f30504a1693cafd26[m
Merge: d2b6030 717785b
Author: Althemar <damien.crechet@gmail.com>
Date:   Wed Apr 11 21:35:05 2018 +0200

    Merge resolved

[33mcommit d2b603057f3ad8c728cb83ec9e3f7e5a8684c73e[m
Author: CRECHET Damien <Damien.crechet@formation.enjmin.fr>
Date:   Wed Apr 11 17:26:38 2018 +0200

    Removed mesh chiant

[33mcommit 717785b4d25a472f8818306d7fe7a07469aa5c23[m
Author: Althemar <damien.crechet@gmail.com>
Date:   Wed Apr 11 15:54:00 2018 +0200

    build ready for ubisoft

[33mcommit 772d92649ff11a0fe75ca1d736c67197825b992f[m
Author: Althemar <damien.crechet@gmail.com>
Date:   Wed Apr 11 15:11:52 2018 +0200

    Sound integrated for ubisoft demo

[33mcommit f037b18ce35fa4528e46c7098186209a40ede30e[m
Author: Althemar <damien.crechet@gmail.com>
Date:   Wed Apr 11 14:28:11 2018 +0200

    Refactoring ubi scene

[33mcommit 05320f59a480f56a758b5068645f03209d82c32c[m
Author: Althemar <damien.crechet@gmail.com>
Date:   Wed Apr 11 01:28:55 2018 +0200

    Fix new wheel

[33mcommit e4a1f150ac02d71f716a0b76645cc150c78a6821[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Tue Apr 10 19:19:58 2018 +0200

    Correcting rotation

[33mcommit 92a38300e6f0ff55070b83984af5ad2d5051fae8[m
Merge: a82c094 91203ec
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Tue Apr 10 18:42:18 2018 +0200

    Merge branch 'master' of https://gitlab.com/neidam/codex

[33mcommit a82c094130893d72bff22256c07db9f3afe03590[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Tue Apr 10 18:42:14 2018 +0200

    test wheel

[33mcommit 91203ec76cbfaaafff3ed74e015ae204a9917fa1[m
Author: Sikblast <robin.cugniet@gmail.com>
Date:   Tue Apr 10 18:09:22 2018 +0200

    gitignore modified

[33mcommit a28ea6b94b3909625741ec816542d494c2f5d148[m
Author: CUGNIET Robin <Robin.cugniet@formation.enjmin.fr>
Date:   Tue Apr 10 18:01:52 2018 +0200

    gitignoremodified

[33mcommit 936cc8112c683b94c761fc5b923db995a0595615[m
Author: Althemar <damien.crechet@gmail.com>
Date:   Tue Apr 10 17:29:49 2018 +0200

    Wwise integrated

[33mcommit 4a69a8aee248c942fc9524d780cabf67bf2bf211[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Tue Apr 10 16:47:57 2018 +0200

    New wheel

[33mcommit db330d42f7b7417ac24063414619aeb22cba8a22[m
Merge: 33a1571 0022200
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Tue Apr 10 15:06:22 2018 +0200

    Merge branch 'damien' into development

[33mcommit 0022200d1ece457ceea67a00f4b27336edbe5b5c[m
Merge: 5326158 3a0b88a
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Tue Apr 10 15:03:58 2018 +0200

    Merge branch 'damien' of https://gitlab.com/neidam/codex into damien

[33mcommit 53261582a40f5ea53843fccba1dba13263c39ec3[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Tue Apr 10 15:03:36 2018 +0200

    New wheel model

[33mcommit 33a15710277599671861347f4265b8d848a5dee6[m
Merge: 975bd3b 3a0b88a
Author: Damien CRECHET <damien.crechet@etu.univ-orleans.fr>
Date:   Fri Apr 6 22:27:38 2018 +0000

    Merge branch 'damien' into 'development'
    
    Test pull request
    
    See merge request neidam/codex!1

[33mcommit 3a0b88a4520ccf0b8a25b886b0b01dc2ddcf4747[m
Author: Althemar <damien.crechet@gmail.com>
Date:   Sat Apr 7 00:20:57 2018 +0200

    Test pull request

[33mcommit 975bd3b7bda28f36c7c50b3566ed22a175f09580[m
Merge: 3e7438d a5a2f8e
Author: Althemar <damien.crechet@gmail.com>
Date:   Fri Apr 6 23:40:13 2018 +0200

    Merge branch 'damien' into development

[33mcommit a5a2f8eaf68246922cfaced56653a1ce54db7da5[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Fri Apr 6 17:34:54 2018 +0200

    Added AR window test with house

[33mcommit 4235b21a43a63ed754c6b69b0ff7fa85e231c4b8[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Fri Apr 6 15:00:07 2018 +0200

    Improved inputs handler. Can now test in editor mode

[33mcommit 3e7438d22402cbb3f5dce7c2a0e59ac8bb8820bd[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Thu Apr 5 15:14:06 2018 +0200

    .gitignore is now working

[33mcommit 290a7c500d31d8f1f011def8da9d094a9b5b1677[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Thu Apr 5 15:13:18 2018 +0200

    Corrected puzzle wheel bug

[33mcommit 62808472ef4126eb17f9e2408ca86712bfed11ff[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Wed Apr 4 15:42:15 2018 +0200

    Objects not visible when required are not validated

[33mcommit 5401dee0be2785a6d64873afc7b3c6bd660df8c9[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Wed Apr 4 13:25:32 2018 +0200

    Added wheel puzzle validation

[33mcommit e848da5c227b6c51c0b3b7cb21c815a2353e3812[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Tue Apr 3 18:38:41 2018 +0200

    Added snapping in ring puzzle

[33mcommit 21d65fb8e5de9f459f1ef766553341e9da4bf48c[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Fri Mar 30 18:16:12 2018 +0200

    Added wheel puzzle

[33mcommit 15e2df4eb1abbd645d313181655c3f1d05ff2a72[m
Author: Damien CRECHET <damien.crechet@gmail.com>
Date:   Wed Mar 28 17:44:50 2018 +0200

    Circle rotation

[33mcommit 60ab84a09a793f58c7f5bd2a2c8fbe406166968a[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Mon Mar 26 18:21:14 2018 +0200

    truc

[33mcommit 662815b648aaf818c62679f7d5d5339e39ed599d[m
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Thu Mar 22 18:12:00 2018 +0100

    TouchHandler added

[33mcommit 2199fc073ba1a5f02baed2148bd4ccb6b15eb268[m
Merge: 3a3b4cb 07b8223
Author: neidam <damien.crechet@etu.univ-orleans.fr>
Date:   Wed Mar 21 19:18:23 2018 +0100

    Merge branch 'master' of https://gitlab.com/neidam/codex

[33mcommit 3a3b4cbdda5f1fac404bbb11225079ab63a5a011[m
Author: CRECHET Damien <Damien.crechet@formation.enjmin.fr>
Date:   Wed Mar 21 19:17:08 2018 +0100

    Added debug log

[33mcommit 07b8223d3388be432ce20a32cb882e799df9291e[m
Author: Damien CRECHET <damien.crechet@gmail.com>
Date:   Wed Mar 21 12:11:18 2018 +0100

    Modified .gitignore

[33mcommit 51dc93b96cf002d6978b38a6e388b593b6a4fc60[m
Author: CRECHET Damien <Damien.crechet@formation.enjmin.fr>
Date:   Tue Mar 20 20:13:20 2018 +0100

    Adde debug, rotation

[33mcommit 4406613a12b4caa2481656f6fbfb704d846cc02f[m
Author: CRECHET Damien <Damien.crechet@formation.enjmin.fr>
Date:   Mon Mar 19 15:32:49 2018 +0100

    Created Unity project

[33mcommit c186bb8d4aaf566f6c88386631a5f53feb89317c[m
Author: CRECHET Damien <Damien.crechet@formation.enjmin.fr>
Date:   Mon Mar 19 15:18:55 2018 +0100

    First commit. Added Readme and .gitignore
